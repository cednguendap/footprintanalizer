from entities.fvideo import FVideo
from processing.fconstante import FImageSize


class VideoProcessing:
    """
    Cette classe posséde des méthodes permettant de manipuler les vidéo
    """
    def scale(video,width,height):
        nvideo = FVideo()
        nvideo.setVideo(video.getVideo())
        nvideo.getVideo().set(FImageSize.FRAME_WIDTH.value, width)
        nvideo.getVideo().set(FImageSize.FRAME_HEIGHT.value, height)
        return nvideo