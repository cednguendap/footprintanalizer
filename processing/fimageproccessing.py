import cv2

from entities.fimage import FImage
from processing.fconstante import FImageColor,FImageSize


class ImageProccessing():
    """
    Cette classe permet de faire le traitement d'image
    Elle contient un ensemble de methode static permettant
    d'appliquer certaines effet sur une images
    """

    def rgb2gray(image):
       return FImage(cv2.cvtColor(image.getImage(), FImageColor.RGB2GRAY.value))


    def brg2rgb(image):
        return FImage(cv2.cvtColor(image.getImage(), FImageColor.BGR2RGB.value))
    def scale(image,width,height):
        nimage=FImage(image.getImage())
        nimage.getImage().set(FImageSize.FRAME_WIDTH, width)
        nimage.getImage().set(FImageSize.FRAME_HEIGHT, height)
        return nimage
