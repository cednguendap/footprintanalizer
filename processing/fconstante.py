import enum
import cv2


class FImageColor(enum.Enum):
    BGR2GRAY = cv2.COLOR_BGR2GRAY
    BGR2RGB = cv2.COLOR_BGR2RGB
    RGB2GRAY = cv2.COLOR_RGB2GRAY


class FImageSize(enum.Enum):
    FRAME_WIDTH = cv2.CAP_PROP_FRAME_WIDTH
    FRAME_HEIGHT = cv2.CAP_PROP_FRAME_HEIGHT
