class FImage:
    """
    Cette classe represente une image et posséde l'ensemble des méthodes
    permettant de manipuler une image
    """

    def __init__(self,frame):
        self.image=frame

    def getImage(self):
        return self.image

    def setImage(self,image):
        self.image=image

    def width(self):
        return self.image.shape[0]

    def height(self):
        return  self.image.shape[1]

    def channel(self):
        if self.isGray() is True:
            return 1
        return self.image.shape[2]

    def size(self):
        return (self.image.shape[0],self.image.shape[1])

    def shape(self):
        return self.image.shape

    def isGray(self):
        return len(self.image.shape)==2

    def get(self,x,y):
        return  self.image[x,y]

    def set(self,x,y,pixel):
        if(self.isGray()==False and len(pixel)!=3):
                raise ValueError("L'image est sur couleur et donc le pixel doit etre définis avec 3 valeurs")
        self.image[x,y]=pixel
