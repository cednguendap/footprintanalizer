import cv2

from entities.fimage import FImage


class FVideo:
    """"
    Cette classe represente la vidéo qui est joué
    elle permet de faire des traitements simple sur une vidéo
    """
    def __init__(self):
        self.video=None
        self.urlFile=""
        self.camera=0
        self.fromcamera=False
        self.freq=0.04 #trouvé en faisant 1/25 car on a 25 images/second
        self.currentImage = None

    def getVideo(self):
        return self.video

    def setVideo(self,video):
        self.video=video

    def videoFromCamera(self,cam):
        self.fromcamera=True
        self.camera=cam

    def videoFromFile(self,urlfile):
        self.fromcamera=False
        self.urlFile=urlfile

    def read(self):
        self.currentImage = None
        if(self.fromcamera):
            self.video=cv2.VideoCapture(self.camera)
        else:
            self.video=cv2.VideoCapture(self.urlFile)
        if not self.video.isOpened():
            raise ValueError("Unnable to open video")

    def next(self):
        if(self.video is None):
            raise RuntimeError("Cannot read video when is ended")
        ok = True
        ok, frame= self.video.read()
        if not ok:
            raise RuntimeError("Can't receive frame")

        if frame is not None:
            self.currentImage = FImage(frame)
        else:
            self.end()
            self.currentImage = None
        return self.currentImage

    def end(self):
        self.video=None
        self.currentImage = None

    def frequency(self):
        return self.freq

    def getCurrentImage(self):
        return self.currentImage