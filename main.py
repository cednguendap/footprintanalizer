#!/usr/bin/env python

# Press the green button in the gutter to run the script.
from ui.main_window import Ui_MainWindow

if __name__ == '__main__':
 ui=Ui_MainWindow()
 ui.start()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
