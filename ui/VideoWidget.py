from PySide2.QtCore import QObject, QFile, QIODevice, QThread,Qt
from PySide2.QtGui import QImage, QPixmap
from PySide2.QtMultimedia import QMediaPlayer
from PySide2.QtMultimediaWidgets import QVideoWidget
import cv2
import time
from PySide2.QtWidgets import QLabel


from processing.fvideoprocessing import VideoProcessing as videoprocess
from ui.ImageWidget import ImageWidget


class VideoWidget(QThread):
    """
    Cette classe permet d'inserer une video dans le frame de la vidéo
    et en fonction du statut de la vidéo
    0:etat stop(arreter)
    1:etat runng (en cours de lecture)
    2:etat pause (en pause)
    """
    RUNNING_STATE = 1
    PAUSE_STATE = 2
    STOP_STATE = 0

    def __init__(self, videoFrame,video=None):
        super().__init__()
        self.videoComponent = ImageWidget()
        self.videoComponent.setParent(videoFrame)
        self.videoComponent.setFixedWidth(videoFrame.frameRect().width())
        self.videoComponent.setFixedHeight(videoFrame.frameRect().height())

        self.video=video
        self.videoState=VideoWidget.STOP_STATE

    def start(self, priority=QThread.InheritPriority):
        self.video.read()
        super().start(priority)

    def run(self):
        #videoprocess.scale(self.video, self.videoComponent.width(), self.videoComponent.height())
        fimage=self.video.next()
        while self.videoState == VideoWidget.RUNNING_STATE and fimage is not None:
            self.videoComponent.showImage(fimage)
            time.sleep(self.video.frequency())
            fimage=self.video.next()


    def show(self):
        self.videoComponent.show()
    def stop(self):
        self.videoState = VideoWidget.STOP_STATE

    def pause(self):
        self.videoState = VideoWidget.PAUSE_STATE

    def resume(self):
        self.videoState = VideoWidget.RUNNING_STATE
        super().start(QThread.InheritPriority)


    def setVideo(self, video):
        self.video=video
        self.resume()

    def getVideo(self):
        return self.video

    def state(self):
        return self.videoState