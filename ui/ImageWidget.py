from PySide2.QtGui import QImage, QPixmap
from PySide2.QtWidgets import QLabel
from PySide2.QtCore import Qt

from entities.fimage import FImage
from processing.fimageproccessing import ImageProccessing as imgprocess

class ImageWidget(QLabel):

    """
    Permet d'inserer une image dans l'interface graphique
    """
    def __init__(self):
        super().__init__()
        self.selectedImage = None


    def showImage(self,image):
        if image.isGray():
            self.showGrayImage(image)
        else:
            self.showRGBImage(image)

    def showRGBImage(self,image):
        self.selectedImage = imgprocess.brg2rgb(image)
        h, w, ch = self.selectedImage.shape()
        bytesPerLine = ch * w
        image = QImage(self.selectedImage.getImage().data, w, h, bytesPerLine, QImage.Format_RGB888)
        imageScaled = FImage(image.scaled(self.parent().width(), self.parent().height(), Qt.KeepAspectRatio))
        self.setPixmap(QPixmap.fromImage(imageScaled.getImage()))

    def showGrayImage(self,image):
        self.selectedImage =image
        h,w = self.selectedImage.shape()
        image = QImage(self.selectedImage.getImage().data, w, h,QImage.Format_Grayscale8);
        imageScaled = FImage(image.scaled(self.parent().width(), self.parent().height(), Qt.KeepAspectRatio))
        self.setPixmap(QPixmap.fromImage(imageScaled.getImage()))

    def getSelectedImage(self):
        return self.selectedImage